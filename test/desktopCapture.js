var WebRTC = require('../');
var PNG = require('pngjs').PNG;
var fs = require('fs');

console.log(WebRTC);

WebRTC.setDebug(true);

var dc = new WebRTC.RTCDesktopCapturer({
    allow_directx_capturer: true
});
dc.start()
console.log('getSourceList', dc.getSourceList());
console.log(dc);

dc.selectSource(0);

dc.onframe = function(frame) {
    console.log('width = ' + frame.width);
    console.log('height = ' + frame.height);
    console.log('stride = ' + frame.stride);
    dc.stop();

    var png = new PNG({
        filterType: -1,
        width: frame.width,
        height: frame.height,
    });

    console.log('writing frame.png');
    png.data = new Uint8Array(frame.data);
    png.pack()
    .pipe(fs.createWriteStream(__dirname + '/../frame.png'))
}
dc.captureFrame();
