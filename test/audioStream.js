var WebRTC = require('../');
var PNG = require('pngjs').PNG;
var fs = require('fs');

WebRTC.setDebug(true);

var localStream;
var timeout;
var renderer = new WebRTC.RTCAudioRenderer();

function clamp(n,low,high){
  if(n<low){return(low);}
  if(n>high){return(high);}
  return parseInt(n);
}

function onSuccess(stream) {
  localStream = stream;
  var audio_list = stream.getAudioTracks();
  renderer.ondata = function(frame) {
    renderer.ondata = null;
    console.log('ondata: ' + JSON.stringify(data, null, 2));
  }

  audio_list.forEach(function (track) {
    console.log('Audio Track');
    track.addRenderer(renderer);
  });
}

var constraints = {
  audio: true,
  video: true,
};

WebRTC.getUserMedia(constraints, onSuccess);

timeout = setTimeout(function() {
  console.log('waiting for frame...');
}, 10000)
