var WebRTC = require('../');
var PNG = require('pngjs').PNG;
var fs = require('fs');

// WebRTC.setDebug(true);

var localStream;
var timeout;
var renderer = new WebRTC.RTCVideoRenderer();

function clamp(n,low,high){
  if(n<low){return(low);}
  if(n>high){return(high);}
  return parseInt(n);
}

function onSuccess(stream) {
  localStream = stream;
  var video_list = stream.getVideoTracks();
  renderer.onframe = function(frame) {
    renderer.onframe = null;
    console.log('onframe: ' + JSON.stringify(frame, null, 2));
    console.log('onframe: ' + frame.width + 'x' + frame.height);
    console.log('onframe: y = ' + frame.y.byteLength);
    console.log('onframe: u = ' + frame.u.byteLength);
    console.log('onframe: v = ' + frame.v.byteLength);

    var imageBuffer = [];

    var png = new PNG({
      filterType: -1,
      width: frame.width,
      height: frame.height,
    });

    var YData = new Uint8Array(frame.y);
    var UData = new Uint8Array(frame.u);
    var VData = new Uint8Array(frame.v);
    console.log('created data')

    var idx = 0;
    for (var y = 0; y < frame.height; y++) {
      for (var x = 0; x < frame.width; x++) {
        // console.log(x + "x" + y)
        var Y = YData[y*frame.width + x];
        var uvIndex = parseInt(y/2)*parseInt(frame.width/2) + parseInt(x/2);
        var U = UData[uvIndex] - 128;
        var V = VData[uvIndex] - 128;
        var R = Y + V * 1.4075;
        var G = Y - U * 0.3455 - V * 0.7169;
        var B = Y + U * 1.7790;

        png.data[idx++] = clamp(Math.floor(R),0,255);
        png.data[idx++] = clamp(Math.floor(G),0,255);
        png.data[idx++] = clamp(Math.floor(B),0,255);
        png.data[idx++] = 0xff;
      }
    }

    console.log(__dirname + '/frame.png');
    png.pack()
      .pipe(fs.createWriteStream(__dirname + '/../frame.png'))
      .on('finish', function() {
        console.log('Finished saving frame');

        video_list.forEach(function (track) {
          stream.removeTrack(track);
          console.log('Stop video');
          // track.stop();
        });

        // process.exit();
        clearTimeout(timeout);
        localStream = null;
      })
  }

  video_list.forEach(function (track) {
    console.log('Video Track');
    track.addRenderer(renderer);
  });
}

var constraints = {
  audio: false,
  video: true,
  // video: {
  //   optional: [
  //     {
  //       sourceId: 'desktop',
  //     }
  //   ]
  // },
/*
  audio: {
    optional: [
      {
        googEchoCancellation: true,
        googEchoCancellation2: true,
        googDAEchoCancellation: true,
        googAutoGainControl: true,
        googAutoGainControl2: true,
        googNoiseSuppression: true,
        googNoiseSuppression2: true,
        googHighpassFilter: true,
        googTypingNoiseDetection: true,
        googAudioMirroring: true,
      },
    ],
  },
  video: {
    optional: [
      {
        minAspectRatio: 1.333,
        maxAspectRatio: 1.778,
        maxWidth: 1920,
        minWidth: 320,
        maxHeight: 1080,
        minHeight: 180,
        maxFrameRate: 60,
        minFrameRate: 30,
      },
    ],
  },
  optional: [
    {
      DtlsSrtpKeyAgreement: true,
    },
  ],
  mandatory: {
    OfferToReceiveAudio: true,
    OfferToReceiveVideo: true,
  },
*/
};

WebRTC.getUserMedia(constraints, onSuccess);

timeout = setTimeout(function() {
  console.log('waiting for frame...');
}, 1000)