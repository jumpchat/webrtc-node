var http = require('https');
var fs = require('fs');
var constants = require('./constants')
var mkdirp = require('mkdirp')
var urlParse = require('url').parse
var spawnSync = require('child_process').spawnSync;

function download(option) {
    if (typeof option == 'string') {
        option = urlParse(option);
    }

    return new Promise(function(resolve, reject) {
        var req = http.request(option, function(res) {
            if (res.statusCode == 200) {
                resolve(res);
            } else {
                if ((res.statusCode === 301 || res.statusCode == 302) && res.headers.location) {
                    resolve(download(res.headers.location));
                } else {
                    reject(res.statusCode);
                }
            }
        })
        .on('error', function(e) {
            reject(e);
        })
        .end();
    });
}


var url = constants.WEBRTC_LIB_URL;
var dest = constants.WEBRTC_LIB_RELEASE;
var root = constants.WEBRTC_ROOT_DIR;
var libdir = constants.WEBRTC_LIB_DIR;
var tarball = 'libwebrtc.tar.xz'
if (!fs.existsSync(dest)) {
    console.log(libdir)
    mkdirp(libdir, function(err) {
        if (err) {
            console.log(err);
        } else {
            console.log(' * Downloading ' + url);
            download(url).then(function(stream) {
                var file = fs.createWriteStream(tarball);
                stream.pipe(file);
                stream.on('end', function() {
                    console.log(' * Extracing to ' + root);

                    //spawnSync('tar', ['-xf', tarball, '-C', root]);
                    //fs.unlink(tarball);

                    const decompress = require('decompress');
                    const decompressTarxz = require('decompress-tarxz');

                    decompress(tarball, root, {
                        plugins: [
                            decompressTarxz()
                        ]
                    }).then(() => {
                        console.log('Files decompressed');
                        fs.unlink(tarball);
                    });

                })
            });
        }
    });
}
