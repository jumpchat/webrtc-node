var fs = require('fs');
var os = require('os');
var spawn = require('child_process').spawn;
var path = require('path');
var request = require('request');
var extend = require('util')._extend;

var PLATFORM = os.platform();
var ROOT = path.resolve(__dirname, '..');
var ARCH = os.arch();
var NODEVER = process.version.split('.');
var PACKAGE = require(path.resolve(ROOT, 'package.json'));

NODEVER[2] = 'x';
NODEVER = NODEVER.join('.');

function build() {
  console.log('Building module...');
  
  var nodegyp = path.resolve(ROOT, 'node_modules', 'node-gyp', 'bin', 'node-gyp.js');
  
  if (!fs.existsSync(nodegyp)) {
    nodegyp = path.resolve(ROOT, '..', 'node-gyp', 'bin', 'node-gyp.js');
    
    if (!fs.existsSync(nodegyp)) {      
      throw new Error('Build Failed. Please follow instructions from https://github.com/vmolsa/webrtc-native/wiki/Getting-started#building-from-source');
    }
  }

  var env = extend(process.env, {
    DEPOT_TOOLS_WIN_TOOLCHAIN: 0
  });
  
  var res = spawn('node', [ nodegyp, 'rebuild' ], {
    cwd: ROOT,
    env: env,
    stdio: 'inherit',
  });
  
  res.on('error', function(error) {
    var res = spawn('iojs', [ nodegyp, 'rebuild' ], {
      cwd: ROOT,
      env: env,
      stdio: 'inherit',
    });
  });
}

function test() {
  console.log('Loading module...');
  
  try {
    var WebRTC = require(path.resolve(ROOT, 'build', 'Release', 'webrtc.node'));
    
    setTimeout(function() {
      console.log('Done! :)');
    }, 200);
  } catch (ignored) {
    throw new Error('prebuilt module not working. See the instructions from https://github.com/vmolsa/webrtc-native/wiki/Getting-started#building-from-source for building module from source.');
  }
}

build();
