var fs = require('fs');
var os = require('os');
var spawn = require('child_process').spawn;
var spawnSync = require('child_process').spawnSync;
var path = require('path');
var request = require('request');
var glob = require('glob');
var mkdirp = require('mkdirp');
var constants = require('./constants')

var ROOT = path.resolve(__dirname, '..');

if (!fs.existsSync(ROOT + path.sep + 'build' + path.sep + 'config.gypi')) {
  throw new Error('Run node-gyp rebuild instead of node build.js');
}

var PACKAGE = require(path.resolve(ROOT, 'package.json'));
var WEBRTC_REVISION = constants.WEBRTC_REVISION;
var CHROMIUM = constants.WEBRTC_REPO;

var USE_OPENSSL = false;
var USE_GTK = false;
var USE_X11 = false;

console.log('args = ' + process.argv)

var PLATFORM = os.platform();
var SYSTEM = os.release();
var ARCH = process.argv[2].substring(14);
var NODEJS = path.resolve(process.argv[3]);
var NODELIB = process.argv[4].substring(3).split('.')[0];
var NODEGYP = process.argv[5];
var NODEVER = process.version.split('.');
var NODE_ZERO = (NODEVER[0] === 'v0');
var CROSSCOMPILE = (ARCH !== process.arch);

NODEVER[2] = 'x';
NODEVER = NODEVER.join('.');
  
if (fs.existsSync(ROOT + path.sep + 'nodejs.gypi')) {
  fs.unlinkSync(ROOT + path.sep + 'nodejs.gypi');
}

var COMMON = path.resolve(NODEJS, 'include', 'node', 'common.gypi');

if (fs.existsSync(COMMON)) {
  fs.linkSync(COMMON, ROOT + path.sep + 'nodejs.gypi');
} else {
  fs.linkSync(NODEJS + path.sep + 'common.gypi', ROOT + path.sep + 'nodejs.gypi');
}

var CONFIG = 'Release';

if (fs.existsSync(ROOT + path.sep + 'build' + path.sep + 'Debug')) {
  CONFIG = 'Debug';
}

var DEPOT_TOOLS_REPO = constants.DEPOT_TOOLS_REPO
var DEPOT_TOOLS = constants.DEPOT_TOOLS_DIR
var WEBRTC = constants.WEBRTC_DIR
var WEBRTC_SRC = path.resolve(WEBRTC, 'src');
var WEBRTC_OUT = path.resolve(WEBRTC_SRC, 'out', CONFIG);
var WEBRTC_BUILD_ARGS = '--args=is_component_build=false rtc_include_tests=false is_debug=' + (CONFIG == 'Debug') + ' target_cpu=\"' + ARCH + '"'
var WEBRTC_LIB_DIR = path.resolve(WEBRTC, 'lib');
var WEBRTC_LIB = path.resolve(WEBRTC_LIB_DIR, (os.platform() == 'win32') ? 'libwebrtc.lib' : 'libwebrtc.a');
var OBJEXT = (os.platform() == 'win32') ? '.obj' : '.o';
var FETCH = path.resolve(DEPOT_TOOLS, (os.platform() == 'win32') ? 'fetch.bat' : 'fetch');
var GCLIENT = path.resolve(DEPOT_TOOLS, (os.platform() == 'win32') ? 'gclient.bat' : 'gclient');
var GN = path.resolve(DEPOT_TOOLS, (os.platform() == 'win32') ? 'gn.bat' : 'gn');

if (os.platform() == 'win32' && ARCH == 'x64') {
  WEBRTC_OUT = path.resolve(WEBRTC_SRC, 'out', CONFIG + '_x64');
}

function compile() {
  var res = spawn('ninja', [ '-C', WEBRTC_OUT, 'examples' ], {
    cwd: WEBRTC_SRC,
    env: process.env,
    stdio: 'inherit',
  });

  res.on('close', function (code) {
    if (!code) {
    //  return install();
      return link();
    }

    process.exit(1);
  });
}

function link() {
  if (!fs.existsSync(WEBRTC_LIB_DIR)) {
    fs.mkdirSync(WEBRTC_LIB_DIR);
  }
  var objFiles = glob(WEBRTC_OUT + '/obj/**/*.o?(bj)', {
    //'ignore': [ '.*examples.*', '.*protouf/protobuf_full.*', '.*protouf/protoc.*', '.*yasm.*' ],
    'ignore': ['**/examples/**', '**/protobuf_full/**', '**/protoc**', '**yasm/gen**', '**yasm/re2c**', '**yasm/yasm**' ]
  }, function(er, files) {
    console.log('Linking webrtc...')    
    var platform = os.platform();
    var fileList = WEBRTC_OUT + path.sep + 'libwebrtc_objs.txt';
    var fd = fs.openSync(fileList, 'w');
    fs.writeSync(fd, files.join('\n'));
    fs.closeSync(fd);
    var options = {
        cwd: WEBRTC_SRC,
        env: process.env,
        stdio: 'inherit',      
    }
    if (platform == 'linux') {
      var res = spawnSync('ar', [].concat('cq', WEBRTC_LIB, files), options);
      res = spawnSync('ranlib', [ WEBRTC_LIB ], options);
    } 
    if (platform == 'darwin') {
      var res = spawnSync('libtool', [].concat('-static', '-o', WEBRTC_LIB, '-no_warning_for_no_symbols', '-filelist', fileList),
        options);
    }
    if (platform == 'win32') {
      console.log('lib.exe', [].concat('/out:' + WEBRTC_LIB, '@'+fileList))
      var res = spawnSync('lib.exe', [].concat('/out:' + WEBRTC_LIB, '@'+fileList),
        options);
    }
  });
}

function build() { 
  if (fs.existsSync(WEBRTC_SRC)) {
    var res = spawn(GN, [ 'gen', WEBRTC_OUT, WEBRTC_BUILD_ARGS ], {
      cwd: WEBRTC_SRC,
      env: process.env,
      stdio: 'inherit',
    });
  
    res.on('close', function (code) {
      if (!code) {
        return compile();
      }
  
      process.exit(1);
    });
  }
}

function sync() {
  if (!fs.existsSync(WEBRTC + path.sep + 'webrtc_sync')) {
    console.log(GCLIENT, ['sync', '--revision', WEBRTC_REVISION, '--reset', '--no-history', '-D']);
    var res = spawn(GCLIENT, ['sync', '--revision', WEBRTC_REVISION, '--reset', '--no-history', '-D'], {
      cwd: WEBRTC,
      env: process.env,
      stdio: 'inherit',
    });

    res.on('close', function (code) {
      if (!code) {
        fs.closeSync(fs.openSync(WEBRTC + path.sep + 'webrtc_sync', 'w'));
        return build();
      }

      process.exit(1);
    });
  } else {
    build();
  }
}

function config() {
  if (!fs.existsSync(WEBRTC) || !fs.existsSync(path.resolve(WEBRTC, '.gclient')) || !fs.existsSync(WEBRTC_SRC)) {
    mkdirp(WEBRTC, function() {
      console.log(GCLIENT, ['config', '--unmanaged', '--name=src', CHROMIUM]);
      var res = spawn(GCLIENT, ['config', '--unmanaged', '--name=src', CHROMIUM], {
        cwd: WEBRTC,
        env: process.env,
        stdio: 'inherit',
      });

      res.on('close', function (code) {
        if (!code) {
          return sync();
        }

        process.exit(1);
      });
    });
  } else {
    sync();
  }
}

process.env['GYP_DEFINES'] = process.env['GYP_DEFINES'] ? process.env['GYP_DEFINES'] : '';

process.env['PATH'] = process.env['PATH'] + path.delimiter + DEPOT_TOOLS;

if (!fs.existsSync(DEPOT_TOOLS)) {
  var res = spawn('git', ['clone', DEPOT_TOOLS_REPO, DEPOT_TOOLS], {
    env: process.env,
    stdio: 'inherit',
  });

  res.on('close', function (code) {
    if (!code) {
      return config();
    }

    process.exit(1);
  });
} else {
  config();
}
