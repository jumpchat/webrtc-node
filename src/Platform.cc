
/*
* The MIT License (MIT)
*
* Copyright (c) 2015 vmolsa <ville.molsa@gmail.com> (http://github.com/vmolsa)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*
*/

#include "Platform.h"

#if defined(WEBRTC_WIN)
#include <rtc_base/win32socketinit.h>
#include <rtc_base/win32socketserver.h>
#endif

using namespace WebRTC;

#ifndef WEBRTC_THREAD_COUNT
#define WEBRTC_THREAD_COUNT 4
#endif

static std::unique_ptr<rtc::Thread> signal_thread;
static std::unique_ptr<rtc::Thread> worker_thread;
static std::unique_ptr<rtc::Thread> network_thread;
static rtc::scoped_refptr<webrtc::PeerConnectionFactoryInterface> factory;

void Platform::Init()
{
    RTC_LOG(LS_INFO) << __PRETTY_FUNCTION__;

#if defined(WEBRTC_WIN)
    rtc::EnsureWinsockInit();
#endif

    rtc::InitializeSSL();

    network_thread = rtc::Thread::CreateWithSocketServer();
    network_thread->Start();

    worker_thread = rtc::Thread::Create();
    worker_thread->Start();

    signal_thread = rtc::Thread::Create();
    signal_thread->Start();

    factory = webrtc::CreatePeerConnectionFactory(
        network_thread.get(),
        worker_thread.get(),
        signal_thread.get(),
        nullptr,
        webrtc::CreateBuiltinAudioEncoderFactory(),
        webrtc::CreateBuiltinAudioDecoderFactory(),
        nullptr,
        nullptr);
}

void Platform::Dispose()
{
    RTC_LOG(LS_INFO) << __PRETTY_FUNCTION__;

    factory = nullptr;

    signal_thread->Stop();
    worker_thread->Stop();
    network_thread->Stop();

    signal_thread = nullptr;
    worker_thread = nullptr;
    network_thread = nullptr;

    rtc::CleanupSSL();
}

rtc::scoped_refptr<webrtc::PeerConnectionFactoryInterface> Platform::GetFactory()
{
    return factory;
}
