#include "DesktopCapturer.h"

using namespace v8;
using namespace WebRTC;

Nan::Persistent<Function> DesktopCapturer::constructor;

void DesktopCapturer::Init(v8::Handle<v8::Object> exports)
{
    RTC_LOG(LS_INFO) << __PRETTY_FUNCTION__;

    Nan::HandleScope scope;

    Local<FunctionTemplate> tpl = Nan::New<FunctionTemplate>(DesktopCapturer::New);
    tpl->InstanceTemplate()->SetInternalFieldCount(1);
    tpl->SetClassName(Nan::New("DesktopCapturer").ToLocalChecked());

    Nan::SetPrototypeMethod(tpl, "start", DesktopCapturer::Start);
    Nan::SetPrototypeMethod(tpl, "stop", DesktopCapturer::Stop);
    Nan::SetPrototypeMethod(tpl, "getSourceList", DesktopCapturer::GetSourceList);
    Nan::SetPrototypeMethod(tpl, "selectSource", DesktopCapturer::SelectSource);
    Nan::SetPrototypeMethod(tpl, "captureFrame", DesktopCapturer::CaptureFrame);
    Nan::SetAccessor(tpl->InstanceTemplate(), Nan::New("onframe").ToLocalChecked(), DesktopCapturer::GetOnFrame, DesktopCapturer::SetOnFrame);

    constructor.Reset<Function>(tpl->GetFunction());
    exports->Set(Nan::New("RTCDesktopCapturer").ToLocalChecked(), tpl->GetFunction());
}

DesktopCapturer::DesktopCapturer()
{
    RTC_LOG(LS_INFO) << __PRETTY_FUNCTION__;
}

DesktopCapturer::~DesktopCapturer()
{
    RTC_LOG(LS_INFO) << __PRETTY_FUNCTION__;
}

void DesktopCapturer::New(const Nan::FunctionCallbackInfo<Value>& info)
{
    RTC_LOG(LS_INFO) << __PRETTY_FUNCTION__;

    Nan::HandleScope scope;

    if (info.IsConstructCall()) {
        DesktopCapturer* capturer = new class DesktopCapturer();
        webrtc::DesktopCaptureOptions options = webrtc::DesktopCaptureOptions::CreateDefault();
        if (info.Length() && info[0]->IsObject()) {
#ifdef WIN32
            Local<Object> localOption = Local<Object>::Cast(info[0]);
            Local<Value> allow_directx_capturer_value = localOption->Get(Nan::New("allow_directx_capturer").ToLocalChecked());
            Local<Int32> allow_directx_capturer(allow_directx_capturer_value->ToInt32(Nan::GetCurrentContext()).ToLocalChecked());
            RTC_LOG(LS_INFO) << "allow_directx_capturer = " << allow_directx_capturer->Value();
            options.set_allow_directx_capturer(allow_directx_capturer->Value());
#endif
        }
        capturer->_capturer = webrtc::DesktopCapturer::CreateScreenCapturer(options);

        capturer->Wrap(info.This(), "DesktopCapturer");
        return info.GetReturnValue().Set(info.This());
    }

    Nan::ThrowError("Internal Error");
    info.GetReturnValue().SetUndefined();
}

void DesktopCapturer::Start(const Nan::FunctionCallbackInfo<v8::Value>& info)
{
    RTC_LOG(LS_INFO) << __PRETTY_FUNCTION__;
    DesktopCapturer* self = RTCWrap::Unwrap<DesktopCapturer>(info.Holder(), "DesktopCapturer");
    self->_capturer->Start(self);
}

void DesktopCapturer::Stop(const Nan::FunctionCallbackInfo<v8::Value>& info)
{
    RTC_LOG(LS_INFO) << __PRETTY_FUNCTION__;
    DesktopCapturer* self = RTCWrap::Unwrap<DesktopCapturer>(info.Holder(), "DesktopCapturer");
    self->_capturer = NULL;
}

void DesktopCapturer::CaptureFrame(const Nan::FunctionCallbackInfo<v8::Value>& info)
{
    RTC_LOG(LS_INFO) << __PRETTY_FUNCTION__;
    DesktopCapturer* self = RTCWrap::Unwrap<DesktopCapturer>(info.Holder(), "DesktopCapturer");
    self->_capturer->CaptureFrame();
}

void DesktopCapturer::GetSourceList(const Nan::FunctionCallbackInfo<v8::Value>& info)
{
    RTC_LOG(LS_INFO) << __PRETTY_FUNCTION__;
    DesktopCapturer* self = RTCWrap::Unwrap<DesktopCapturer>(info.Holder(), "DesktopCapturer");
    webrtc::DesktopCapturer::SourceList sourceList;
    bool result = self->_capturer->GetSourceList(&sourceList);
    Local<Array> list = Nan::New<Array>();
    if (result) {
        for (size_t i = 0; i < sourceList.size(); i++) {
            webrtc::DesktopCapturer::Source src = sourceList[i];
            Local<Object> source = Nan::New<Object>();
            source->Set(Nan::New("id").ToLocalChecked(), Nan::New((uint32_t)src.id));
            source->Set(Nan::New("title").ToLocalChecked(), Nan::New(src.title).ToLocalChecked());
            list->Set(i, source);
        }
    }

    info.GetReturnValue().Set(list);
}

void DesktopCapturer::SelectSource(const Nan::FunctionCallbackInfo<v8::Value>& info)
{
    RTC_LOG(LS_INFO) << __PRETTY_FUNCTION__;
    DesktopCapturer* self = RTCWrap::Unwrap<DesktopCapturer>(info.Holder(), "DesktopCapturer");

    if (info.Length() == 1 && info[0]->IsInt32()) {
        Local<Int32> id(info[0]->ToInt32(Nan::GetCurrentContext()).ToLocalChecked());
        RTC_LOG(LS_INFO) << "SelectSource " << id->Value();
        self->_capturer->SelectSource(id->Value());
    }
}

void DesktopCapturer::GetOnFrame(Local<String> property, const Nan::PropertyCallbackInfo<Value>& info)
{
    RTC_LOG(LS_INFO) << __PRETTY_FUNCTION__;

    DesktopCapturer* self = RTCWrap::Unwrap<DesktopCapturer>(info.Holder(), "DesktopCapturer");
    return info.GetReturnValue().Set(Nan::New<Function>(self->_onframe));
}

void DesktopCapturer::SetOnFrame(Local<String> property, Local<Value> value, const Nan::PropertyCallbackInfo<void>& info)
{
    RTC_LOG(LS_INFO) << __PRETTY_FUNCTION__;

    DesktopCapturer* self = RTCWrap::Unwrap<DesktopCapturer>(info.Holder(), "DesktopCapturer");

    if (!value.IsEmpty() && value->IsFunction()) {
        self->_onframe.Reset<Function>(Local<Function>::Cast(value));
    } else {
        self->_onframe.Reset();
    }
}

void DesktopCapturer::On(Event* event)
{
    RTC_LOG(LS_INFO) << __PRETTY_FUNCTION__;
}

void DesktopCapturer::OnCaptureResult(webrtc::DesktopCapturer::Result result, std::unique_ptr<webrtc::DesktopFrame> frame)
{
    RTC_LOG(LS_INFO) << __PRETTY_FUNCTION__;

    Local<Function> callback = Nan::New<Function>(_onframe);
    if (!callback.IsEmpty() && callback->IsFunction()) {
        Local<Object> container = Nan::New<Object>();
        Local<Value> argv[] = { container };
        int argc = 1;

        size_t frameSize = frame->size().height()*frame->stride();
        RTC_LOG(LS_INFO) << "size = " << frame->size().width()  << "x" << frame->size().height();
        RTC_LOG(LS_INFO) << "stride = " << frame->stride();
        RTC_LOG(LS_INFO) << "frameSize = " << frameSize;

        Local<ArrayBuffer> data = ArrayBuffer::New(v8::Isolate::GetCurrent(), frameSize);
        memcpy(data->GetContents().Data(), frame->data(), frameSize);

        container->Set(Nan::New("data").ToLocalChecked(), data);
        container->Set(Nan::New("stride").ToLocalChecked(), Nan::New(frame->stride()));
        container->Set(Nan::New("width").ToLocalChecked(), Nan::New(frame->size().width()));
        container->Set(Nan::New("height").ToLocalChecked(), Nan::New(frame->size().height()));

        callback->Call(RTCWrap::This(), argc, argv);
    }
}
