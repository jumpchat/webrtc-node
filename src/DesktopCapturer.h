
#ifndef WEBRTC_DESKTOPCAPTURER_H
#define WEBRTC_DESKTOPCAPTURER_H

#include "Common.h"
#include "EventEmitter.h"
#include "Observers.h"
#include "Wrap.h"

namespace WebRTC {
class DesktopCapturer : public RTCWrap,
                      public EventEmitter,
                      public webrtc::DesktopCapturer::Callback {
public:
    static void Init(v8::Handle<v8::Object> exports);

private:
    DesktopCapturer();
    ~DesktopCapturer() final;

    static void New(const Nan::FunctionCallbackInfo<v8::Value>& info);

    static void Start(const Nan::FunctionCallbackInfo<v8::Value>& info);
    static void Stop(const Nan::FunctionCallbackInfo<v8::Value>& info);
    static void CaptureFrame(const Nan::FunctionCallbackInfo<v8::Value>& info);
    static void GetSourceList(const Nan::FunctionCallbackInfo<v8::Value>& info);
    static void SelectSource(const Nan::FunctionCallbackInfo<v8::Value>& info);
    static void GetOnFrame(v8::Local<v8::String> property, const Nan::PropertyCallbackInfo<v8::Value>& info);
    static void SetOnFrame(v8::Local<v8::String> property, v8::Local<v8::Value> value, const Nan::PropertyCallbackInfo<void>& info);

    virtual void OnCaptureResult(webrtc::DesktopCapturer::Result result, std::unique_ptr<webrtc::DesktopFrame> frame);
    void On(Event* event) final;

protected:
    std::unique_ptr<webrtc::DesktopCapturer> _capturer;
    Nan::Persistent<v8::Function> _onframe;

    static Nan::Persistent<v8::Function> constructor;
};
};

#endif
